/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alvarado.webscraping.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author ThinkPad
 */
public class Config {
    public static Properties getXMLInternalProperties(String configFile) throws IOException {
        Properties props = new Properties();
        props.loadFromXML(Config.getStream(configFile));
        return props;
    }
    
    public static InputStream getStream(String file) {
        InputStream input;
        ClassLoader classLoader = Config.class.getClassLoader();
        if(classLoader != null) { //Attempts to open an input stream to the configuration file
            input = classLoader.getResourceAsStream(file);
        } else { //The configuration file is considered to be a system resource
            input = classLoader.getSystemResourceAsStream(file);
        }
        return input;
    }
}
