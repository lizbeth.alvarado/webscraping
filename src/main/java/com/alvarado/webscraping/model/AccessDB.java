/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alvarado.webscraping.model;

import com.alvarado.webscraping.utils.EMFactory;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author ThinkPad
 */
public class AccessDB {

    private final EMFactory emfactory;

    public AccessDB(String persistanceUnit) {
        this.emfactory = new EMFactory(persistanceUnit);
    }

    public void GuardaProducto(String descripcion, String precio, String nombre, Comercio comercio) {
        EntityManager em = null;
        Producto producto = inicializaProducto(descripcion, precio, nombre, comercio);
        try {
            em = emfactory.getEntitymanager();
            TypedQuery<Producto> query = em.createQuery("SELECT p FROM Producto p WHERE p.nombre=:nombre", Producto.class)
                    .setParameter("nombre", producto.getNombre());
            em.getTransaction().begin();
            Producto prod = query.getSingleResult();
            prod.setPrecio(producto.getPrecio());
            prod.setFechaModificacion(producto.getFechaModificacion());
            prod.setIdComercio(producto.getIdComercio());
            em.getTransaction().commit();
        } catch (Exception e) {
            try {
                em = emfactory.getEntitymanager();
                em.getTransaction().begin();
                em.persist(producto);
                em.getTransaction().commit();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    private Producto inicializaProducto(String descripcion, String precio, String nombre, Comercio comercio) {
        Producto producto = new Producto();
        producto.setDescripcion(descripcion);
        producto.setNombre(nombre);
        producto.setFechaModificacion(Calendar.getInstance().getTime());
        producto.setPrecio(new BigDecimal(precio.replace(",", "").replace("$", "")));
        producto.setIdComercio(comercio);
        return producto;
    }

    public List<Producto> GetAllProductos() {
        EntityManager em = null;
        List<Producto> productos = null;
        try {
            em = emfactory.getEntitymanager();
            TypedQuery<Producto> query = em.createNamedQuery("Producto.findAll", Producto.class);
            productos = query.getResultList();
        } catch (Exception e) {
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return productos;
    }
    
    public Comercio getComercioByNombre(String nombre) {
        Comercio comercio = null;
        EntityManager em = null;
        try {
            em = emfactory.getEntitymanager();
            TypedQuery<Comercio> query = em.createQuery("SELECT c FROM Comercio c WHERE c.nombre=:nombre", Comercio.class)
                    .setParameter("nombre", nombre);
            comercio = query.getSingleResult();
        } catch (Exception e) {
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return comercio;
    }
    
    public Comercio getComercioById(int Id) {
        Comercio comercio = null;
        EntityManager em = null;
        try {
            em = emfactory.getEntitymanager();
            TypedQuery<Comercio> query = em.createQuery("SELECT c FROM Comercio c WHERE c.id=:id", Comercio.class)
                    .setParameter("id", Id);
            comercio = query.getSingleResult();
        } catch (Exception e) {
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return comercio;
    }
    
    public List<Comercio> getComercios() {
        List<Comercio> comercios = null;
        EntityManager em = null;
        try {
            em = emfactory.getEntitymanager();
            TypedQuery query = em.createQuery("SELECT c FROM Comercio c ORDER BY c.nombre", Comercio.class);
            comercios = query.getResultList();
        } catch (Exception e) {
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return comercios;
    }
    
    public List<Producto> GetByDate(Date fecha) {
        List<Producto> productos = null;
        EntityManager em = null;
        try {
            em = emfactory.getEntitymanager();
            TypedQuery<Producto> query = em.createQuery("SELECT p FROM Producto p WHERE p.fechaModificacion>:fecha", Producto.class)
                    .setParameter("fecha", fecha);
            productos = query.getResultList();
        } catch (Exception e) {
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return productos;
    }
}
