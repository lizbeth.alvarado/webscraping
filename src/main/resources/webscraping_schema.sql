--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2019-08-29 09:40:43

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2826 (class 1262 OID 16437)
-- Name: webscraping; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE webscraping WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';


ALTER DATABASE webscraping OWNER TO postgres;

\connect webscraping

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 198 (class 1259 OID 16498)
-- Name: comercios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comercios (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL
);


ALTER TABLE public.comercios OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16501)
-- Name: comercios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.comercios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comercios_id_seq OWNER TO postgres;

--
-- TOC entry 2827 (class 0 OID 0)
-- Dependencies: 199
-- Name: comercios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.comercios_id_seq OWNED BY public.comercios.id;


--
-- TOC entry 196 (class 1259 OID 16441)
-- Name: productos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.productos (
    id integer NOT NULL,
    descripcion character varying(300),
    nombre character varying(150) NOT NULL,
    precio numeric(14,2) NOT NULL,
    fecha_modificacion timestamp with time zone,
    id_comercio integer
);


ALTER TABLE public.productos OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16444)
-- Name: productos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.productos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productos_id_seq OWNER TO postgres;

--
-- TOC entry 2828 (class 0 OID 0)
-- Dependencies: 197
-- Name: productos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.productos_id_seq OWNED BY public.productos.id;


--
-- TOC entry 2693 (class 2604 OID 16503)
-- Name: comercios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comercios ALTER COLUMN id SET DEFAULT nextval('public.comercios_id_seq'::regclass);


--
-- TOC entry 2692 (class 2604 OID 16446)
-- Name: productos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos ALTER COLUMN id SET DEFAULT nextval('public.productos_id_seq'::regclass);


--
-- TOC entry 2698 (class 2606 OID 16508)
-- Name: comercios comercio_PK; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comercios
    ADD CONSTRAINT "comercio_PK" PRIMARY KEY (id);


--
-- TOC entry 2696 (class 2606 OID 16451)
-- Name: productos productos_PK; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT "productos_PK" PRIMARY KEY (id);


--
-- TOC entry 2694 (class 1259 OID 16514)
-- Name: fki_productos_comercios_FK; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "fki_productos_comercios_FK" ON public.productos USING btree (id_comercio);


--
-- TOC entry 2699 (class 2606 OID 16509)
-- Name: productos productos_comercios_FK; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT "productos_comercios_FK" FOREIGN KEY (id_comercio) REFERENCES public.comercios(id);


-- Completed on 2019-08-29 09:40:43

--
-- PostgreSQL database dump complete
--

