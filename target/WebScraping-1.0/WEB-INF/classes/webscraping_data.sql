--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2019-08-29 09:42:29

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2823 (class 0 OID 16498)
-- Dependencies: 198
-- Data for Name: comercios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.comercios (id, nombre) VALUES (1, 'Amazon');
INSERT INTO public.comercios (id, nombre) VALUES (2, 'Liverpool');
INSERT INTO public.comercios (id, nombre) VALUES (3, 'Linio');



--
-- TOC entry 2830 (class 0 OID 0)
-- Dependencies: 199
-- Name: comercios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.comercios_id_seq', 3, true);


-- Completed on 2019-08-29 09:42:29

--
-- PostgreSQL database dump complete
--

